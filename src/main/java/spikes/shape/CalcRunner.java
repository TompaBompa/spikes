package spikes.shape;

/**
 *
 * @author thomas
 */
public class CalcRunner {

    public static void main(String[] args) {
        var calculator = new GeometryCalculator();
        System.out.println("Circle radius 2,5 -> area " + calculator.calculateArea(new Circle(2.5)));
        System.out.println("Rectangle height 1,7 length 2,1 -> area " + calculator.calculateArea(new Rectangle(2.1, 1.7)));
    }
}

package spikes.shape;

/**
 *
 * @author thomas
 */
public class Rectangle implements Shape {

    private final double length, height;

    public Rectangle(double length, double height) {
        this.length = length;
        this.height = height;
    }

    public double getLength() {
        return length;
    }

    public double getHeight() {
        return height;
    }
    
    @Override
    public double getArea() {
        return height * length;
    }
}

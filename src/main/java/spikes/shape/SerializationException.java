package spikes.shape;

/**
 *
 * @author thomas
 */
public class SerializationException extends RuntimeException {

    public SerializationException(Throwable cause) {
        super(cause);
    }
}

package spikes.shape;

/**
 *
 * @author thomas
 */
public interface GeometrySerializer {

    String serialize(Shape s);
}

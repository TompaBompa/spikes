package spikes.shape;

/**
 *
 * @author thomas
 */
public class Triangle implements Shape {

    private final double base, height;

    public Triangle(double base, double height) {
        this.base = base;
        this.height = height;
    }

    public double getBase() {
        return base;
    }

    public double getHeight() {
        return height;
    }

    @Override
    public double getArea() {
        return base * height / 2.0;
    }
}

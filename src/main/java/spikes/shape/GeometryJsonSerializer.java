package spikes.shape;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 *
 * @author thomas
 */
public class GeometryJsonSerializer implements GeometrySerializer {

    @Override
    public String serialize(Shape s) {
        try {
            return new ObjectMapper().writeValueAsString(s);
        } catch (JsonProcessingException ex) {
            throw new SerializationException(ex);
        }
    }
}

package spikes.shape;

/**
 *
 * @author thomas
 */
public interface Shape {

    double getArea();
}

package spikes.shape;

/**
 *
 * @author thomas
 */
public class SerializeRunner {

    public static void main(String[] args) {
        var serializer = new GeometryJsonSerializer();
        System.out.println("Circle as JSON: " + serializer.serialize(new Circle(2.1)));
        System.out.println("Rectangle as JSON: " + serializer.serialize(new Rectangle(3.2, 1.1)));
    }
}

package spikes.shape;

/**
 *
 * @author thomas
 */
public class GeometryCalculator {

    public double calculateArea(Shape s) {
        return s.getArea();
    }
}

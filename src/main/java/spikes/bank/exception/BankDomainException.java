package spikes.bank.exception;

/**
 *
 * @author thomas
 */
public class BankDomainException extends RuntimeException {

    public BankDomainException(String message) {
        super(message);
    }
}

package spikes.bank.domain;

import java.math.BigDecimal;
import spikes.bank.exception.BankDomainException;

/**
 *
 * @author thomas
 */
public class TransactionAccount implements WithdrawableAccount {

    private BigDecimal balance = BigDecimal.ZERO;

    @Override
    public BigDecimal getBalance() {
        return balance;
    }

    @Override
    public void deposit(BigDecimal amount) {
        balance = balance.add(amount);
    }

    @Override
    public void withdraw(BigDecimal amount) {
        if (amount.compareTo(balance) > 0) {
            throw new BankDomainException("Withdrawal amount exceeds balance");
        }
        balance = balance.subtract(amount);
    }

}

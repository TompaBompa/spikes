package spikes.bank.domain;

import java.math.BigDecimal;
import spikes.bank.exception.BankDomainException;

/**
 *
 * @author thomas
 */
public class FixedTermDepositAccount implements Account {

    private BigDecimal balance = BigDecimal.ZERO;

    @Override
    public BigDecimal getBalance() {
        return balance;
    }

    @Override
    public void deposit(BigDecimal amount) {
        balance = balance.add(amount);
    }
}

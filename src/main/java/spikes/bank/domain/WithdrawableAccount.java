package spikes.bank.domain;

import java.math.BigDecimal;

/**
 *
 * @author thomas
 */
public interface WithdrawableAccount extends Account {

    public void withdraw(BigDecimal amount);
}

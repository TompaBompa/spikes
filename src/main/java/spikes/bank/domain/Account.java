package spikes.bank.domain;

import java.math.BigDecimal;

/**
 *
 * @author thomas
 */
public interface Account {

    public BigDecimal getBalance();

    public void deposit(BigDecimal amount);

}

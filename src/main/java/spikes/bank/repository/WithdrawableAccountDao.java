package spikes.bank.repository;

import spikes.bank.domain.*;
import spikes.bank.exception.BankDomainException;

/**
 *
 * @author thomas
 */
public class WithdrawableAccountDao extends Dao<WithdrawableAccount> {

    @Override
    public WithdrawableAccount getById(Long id) {
        return (WithdrawableAccount) ensureContent(id, this::setupNewAccount);
    }

    private WithdrawableAccount setupNewAccount(Long id) {
        if (id < 100L) {
            throw new BankDomainException("Withdrawable account with id " + id + " missing");
        } else if (id < 200L) {
            return new SavingsAccount();
        }
        return new TransactionAccount();
    }
}

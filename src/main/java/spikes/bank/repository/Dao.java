package spikes.bank.repository;

import java.util.*;
import java.util.function.Function;
import spikes.bank.domain.Account;

/**
 *
 * @author thomas
 */
public abstract class Dao<T> {

    private final static Map<Long, Account> cache = new HashMap<>();

    public abstract T getById(Long id);

    protected Account ensureContent(Long id, Function<Long, ? extends Account> accountCreator) {
        if (!cache.containsKey(id)) {
            cache.put(id, accountCreator.apply(id));
        }
        return cache.get(id);
    }

}

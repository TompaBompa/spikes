package spikes.bank.repository;

import spikes.bank.domain.*;

/**
 *
 * @author thomas
 */
public class AccountDao extends Dao<Account> {
    
    @Override
    public Account getById(Long id) {
        return ensureContent(id, this::setupNewAccount) ;
    }
    
    private Account setupNewAccount(Long id) {
        if (id < 100L) {
            return new FixedTermDepositAccount();
        } else if (id < 200L) {
            return new SavingsAccount();
        }
        return new TransactionAccount();
    }
}

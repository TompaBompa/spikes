package spikes.bank.service;

import java.math.BigDecimal;
import spikes.bank.domain.*;
import spikes.bank.repository.*;

/**
 *
 * @author thomas
 */
public class DepositService {

    private final AccountDao dao = new AccountDao();

    public BigDecimal execute(BigDecimal amount, Long id) {
        Account account = dao.getById(id);
        account.deposit(amount);
        return account.getBalance();
    }
}

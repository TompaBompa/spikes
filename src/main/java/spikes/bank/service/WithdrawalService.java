package spikes.bank.service;

import java.math.BigDecimal;
import spikes.bank.domain.WithdrawableAccount;
import spikes.bank.repository.WithdrawableAccountDao;

/**
 *
 * @author thomas
 */
public class WithdrawalService {

    private final WithdrawableAccountDao dao = new WithdrawableAccountDao();

    public BigDecimal execute(BigDecimal amount, Long id) {
        WithdrawableAccount account = dao.getById(id);
        account.withdraw(amount);
        return account.getBalance();
    }
}

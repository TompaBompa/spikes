package spikes.bank;

import java.math.BigDecimal;
import spikes.bank.service.*;

/**
 *
 * @author thomas
 */
public class Runner {

    public static void main(String[] args) {
        DepositService depositService = new DepositService();
        WithdrawalService withdrawalService = new WithdrawalService();
        System.out.println("Balance after deposit: " + depositService.execute(new BigDecimal(6500), 120L));
        System.out.println("Balance after withdrawal: " + withdrawalService.execute(new BigDecimal(2300), 120L));
    }
}

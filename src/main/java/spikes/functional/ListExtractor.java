package spikes.functional;

import java.util.*;
import static java.util.List.of;
import java.util.stream.Collectors;

/**
 *
 * @author thomas
 */
public class ListExtractor {

    public static void main(String[] args) {
        List<String> source = of("A", "ABC", "ABCD", "XY", "AAA", "ABCDE");
//        List<String> source = of();
        List<Integer> result = new ArrayList<>();
        for (String str : source) {
            if (str.startsWith("A")) {
                result.add(str.length());
            }
        }
        System.out.println(result);
        
        System.out.println(source.stream()
                .filter(s -> s.startsWith("A"))
                .sorted((s1, s2) -> -1)
                .map(s -> s.length())
                .peek(i -> System.out.println(i.getClass().getSimpleName()))
                .reduce((a, b) -> a + b).orElse(0));
        
        System.out.println(source.stream()
                .filter(s -> s.startsWith("A"))
                .sorted((s1, s2) -> -1)
                .map(s -> s.length())
                .peek(i -> System.out.println(i.getClass().getSimpleName()))
                .collect(Collectors.summingInt(i -> i)));
        
        System.out.println(source.stream()
                .filter(s -> s.startsWith("A"))
                .sorted((s1, s2) -> -1)
                .collect(Collectors.toList()));
        
        System.out.println(source.stream()
                .filter(s -> s.startsWith("A"))
                .sorted((s1, s2) -> -1)
                .collect(Collectors.joining(",")));
    }
}

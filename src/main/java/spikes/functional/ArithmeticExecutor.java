package spikes.functional;

/**
 *
 * @author thomas
 */
public class ArithmeticExecutor {

    public Double useExecutionMethod(Operation operation) {
        return operation.execute(12.0, 22.3);
    }

    public static void main(String[] args) {
        System.out.println(new ArithmeticExecutor().useExecutionMethod(new Operation() {
            @Override
            public Double execute(Double a, Double b) {
                return a / b;
            }
        }));
        var me = new ArithmeticExecutor();
        System.out.println(me.useExecutionMethod((a, b) -> a * b));
    }

    public Double multiply(Double a, Double b) {
        return a * b;
    }
}

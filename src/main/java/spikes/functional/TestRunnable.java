package spikes.functional;

import java.util.concurrent.Executor;

/**
 *
 * @author thomas
 */
public class TestRunnable {

    public class ThreadTester implements Runnable {

        @Override
        public void run() {
            System.out.println("This is the ThreadTester class");
        }
    }
    
    private void nonsense() {
        System.out.println("This is the nonsense method");
    }
    
    public static void main(String[] args) {
        new TestRunnable().getItDone();
    }
    
    private void getItDone() {
        Runnable threadTester = new ThreadTester();
        Runnable anonymounClass = new Runnable() {
            @Override
            public void run() {
                System.out.println("This is an anonymous class");
            }
        };
        Runnable nonsenseMethod = this::nonsense;
        Runnable lambdaExpression = () -> System.out.println("This is a lambda expression");
        executeRunnable(threadTester);
        executeRunnable(anonymounClass);
        executeRunnable(nonsenseMethod);
        executeRunnable(lambdaExpression);
    }
    
    private void executeRunnable(Runnable r) {
        r.run();
    }
}

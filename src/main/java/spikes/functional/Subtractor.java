package spikes.functional;

/**
 *
 * @author thomas
 */
public class Subtractor implements Operation {

    @Override
    public Double execute(Double a, Double b) {
        return a - b;
    }
}

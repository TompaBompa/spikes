package spikes.functional;

/**
 *
 * @author thomas
 */
@FunctionalInterface
public interface Operation {

    Double execute(Double a, Double b);
}
